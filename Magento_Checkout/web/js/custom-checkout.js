window.addEventListener('load', function(e) {
    require(['jquery', 'jquery/ui'], function($){ 

       $(document).on("click", "#opc-shipping_method .checkout-shipping-method input.radio", function() {
            if($(this).attr("checked") != true) {
                $(this).attr("checked", true);
                $(".table-checkout-shipping-method tbody tr").removeClass("active");
                $(this).parent().parent().addClass("active");
            }
       })
    });
    
})