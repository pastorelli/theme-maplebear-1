define([
    "jquery",
    "underscore",
    "Magento_Catalog/js/price-utils",
    "jquery/ui",
    "mage/translate"
], function ($, _, utils) {
    'use strict';

    var addToCartExtraJsonData = [];
    var addToCartValidations = [];

    $.widget('mpassignproduct.buybox', {
        options: {},
        _create: function() {
            var self = this;

            $(document).ready(function() {
                self.debug = false;
                debugLog(self.options)

                // Hides product price and original buy button
                $('.price-final_price').hide();
                $('.field.qty').hide();
                $('.field.qty.buybox').show();
                $('#product-addtocart-button').hide();
                $('.wk-seller-card-container').hide();

                // Creates click listener for radio buttons
                $(document).on('click', '.buybox-radio', function(event) {

                    //adding da active class on parent item-container
                    $(".item-container").removeClass("active");
                    $(this).parent().parent().parent().addClass("active");

                    let radioIndex = $(this).data('index');
                    let buyboxHtml = $('#data-item-' + radioIndex).html();
                    $('#buybox-product').html(buyboxHtml);
                });

                // Creates click listener for buy buttons
                $(document).on('click', '.buybox-add-to-cart', function(event) {
                    var buyboxAddToCart = this;

                    let assignId = $(this).attr('data-id');
                    let assignProductId = $(this).attr('assign-product-id');
                    let qty = $('#buybox-product').find('.qty').val();
                    let formkey = getFormKey();
                    let addToCartUrl = $(this).attr('addtocart-url');

                    let jsonData = {};
                    jsonData.mpassignproduct_id = assignId;
                    jsonData.product = assignProductId;
                    jsonData.qty = qty;
                    jsonData.form_key = formkey;
                    addToCartExtraJsonData.forEach(extraJsonDataFunction=> {
                        jsonData = extraJsonDataFunction();
                    });

                    if(self.options.isConfigurable) {
                        if(missingSuperAttributeToSelect())
                            return;

                        let associateId = $(this).attr("data-associate-id");

                        let superAttribute = {};
                        let formData = $('#product_addtocart_form').serializeArray();
                        $.each(formData, function ( key, value ) {
                            let atr = value.name;
                            let val = value.value;
                            if (atr.indexOf('super_attribute') === 0) {
                                atr = atr.replace(/[^\d.]/g, '');
                                superAttribute[atr] = val;
                            }
                        });

                        jsonData.super_attribute = superAttribute;
                        jsonData.associate_id = associateId;
                    }

                    var canAddToCart = true;
                    addToCartValidations.forEach(validationFunction => {
                        canAddToCart = validationFunction(buyboxAddToCart);
                    });

                    if(!canAddToCart) return;

                    displayLoadingMask();
                    $.ajax({
                        url: addToCartUrl,
                        type: 'POST',
                        data: jsonData,
                        success: function (data) {
                            if (data.backUrl) {
                                location.reload();
                            }
                            hideLoadingMask();
                            ascrollto('maincontent');
                        }
                    });
                });

                function displayLoadingMask() {
                    $(".wk-loading-mask").removeClass("wk-display-none");
                }

                function hideLoadingMask() {
                    $(".wk-loading-mask").addClass("wk-display-none");
                }

                function ascrollto(id)
                {
                    var etop = $('#' + id).offset().top;
                    $('html, body').animate({
                        scrollTop: etop-30
                    }, 10);
                }

                function getFormKey()
                {
                    let formkey = '';

                    var formData = $('#product_addtocart_form').serializeArray();
                    $.each(formData,function (i,v) {
                        if (v.name == 'form_key') {
                            formkey = v.value;
                        }
                    });

                    return formkey;
                }

                function missingSuperAttributeToSelect()
                {
                    let anyMissingSuperAttribute = false;

                    $('#product-options-wrapper .super-attribute-select').each(function () {
                        if ($(this).val() == "") {
                            anyMissingSuperAttribute = true;
                        }
                    });

                    return anyMissingSuperAttribute;
                }

                $('#product-options-wrapper .super-attribute-select').change(function () {
                    /**
                     * This timeout is needed for the hidden input
                     * with name selected_configurable_option
                     * update with product data.
                     */
                    setTimeout(function() {
                        debugLog('#product-options-wrapper .super-attribute-select changed');

                        $('.out-of-stock').hide();
                        $('.buybox-add-to-cart').prop('disabled', 'disabled');

                        if(missingSuperAttributeToSelect()) {
                            debugLog('Missing super attributes to select');

                            $('.super-attribute-unselected').show();
                            $('.super-attribute-selected').hide();
                            return;
                        }

                        $('.super-attribute-unselected').hide();
                        $('.super-attribute-selected').show();

                        // Child product id
                        let assignProductId = $('input[name=selected_configurable_option]').val();

                        debugLog('assignProductId ' + assignProductId);

                        $('.buybox-radio').each(function() {
                            let associateProductId = $(this).data('id');
                            let index = $(this).data('index');

                            let price = self.options.prices[assignProductId][associateProductId];

                            if(typeof price == 'undefined')
                                price = $.mage.__("Indisponível");

                            $('#supper-attribute-price-' + index).html(price);
                            $('#data-item-' + index + ' .price-box .buybox-price.super-attribute-selected').html(price);

                            let quantity = 0;

                            if(typeof self.options.quantities[assignProductId][associateProductId] != 'undefined') {
                                quantity = Number(self.options.quantities[assignProductId][associateProductId]);

                                if(quantity > 0)
                                    $('#buybox-add-to-cart-' + index).prop('disabled', false);
                            }

                            $('#out-of-stock-' + index).show();
                        });

                        updateSelectedBuyBox()
                    }, 100);
                });

                $("body").on("click", ".swatch-attribute", onSwatchClick);
                $("body").on("click", ".swatch-option", onSwatchClick);
                function onSwatchClick() {
                    debugLog('Swatch clicked');
                    let selectedOptions = getSwatchesSelectedOptions();
                    debugLog('selectedOptions:');
                    debugLog(selectedOptions);
                    let productIdIndexes = getSwatchesOptionsIndex();
                    debugLog('productIdIndexes:');
                    debugLog(productIdIndexes);

                    $('.out-of-stock').hide();
                    $('.buybox-add-to-cart').prop('disabled', 'disabled');

                    if(missingSwatchesToSelect()) {
                        debugLog('missingSwatchesToSelect');
                        $('.super-attribute-unselected').show();
                        $('.super-attribute-selected').hide();
                        return;
                    }

                    $('.super-attribute-unselected').hide();
                    $('.super-attribute-selected').show();

                    jQuery.each(productIdIndexes, function (productId, attributes) {
                        if(isSwatchesProductSelected(attributes, selectedOptions)) {

                            $('.buybox-radio').each(function() {
                                let associateProductId = $(this).data('id');
                                let index = $(this).data('index');

                                let price = self.options.prices[productId][associateProductId];

                                if(typeof price == 'undefined')
                                    price = $.mage.__("Indisponível");

                                if (typeof price === 'object') {
                                    $('#supper-attribute-price-' + index).html(price['special_price']);
                                    let htmlSpecialPrice = "De "+ price['price'] + "<br>Por " + price['special_price'];
                                    $('#data-item-' + index + ' .price-box .buybox-price.super-attribute-selected').html(htmlSpecialPrice);
                                } else {
                                    $('#supper-attribute-price-' + index).html(price);
                                    $('#data-item-' + index + ' .price-box .buybox-price.super-attribute-selected').html(price);
                                }

                                let quantity = 0;

                                if(typeof self.options.quantities[productId][associateProductId] != 'undefined') {
                                    quantity = Number(self.options.quantities[productId][associateProductId]);

                                    if(quantity > 0) {
                                        $('#buybox-add-to-cart-' + index).prop('disabled', false);
                                        return;
                                    }
                                }

                                $('#out-of-stock-' + index).show();
                            });


                        }
                    });

                    updateSelectedBuyBox()
                }

                function missingSwatchesToSelect() {
                    var missingAny = false;

                    jQuery('div.swatch-attribute').each(function (k,v) {
                        var attribute_id    = jQuery(v).attr('attribute-id');
                        var option_selected = jQuery(v).attr('option-selected');

                        if (!attribute_id || !option_selected) {
                            missingAny = true;
                        }
                    });

                    return missingAny;
                }

                function getSwatchesSelectedOptions() {
                    var selectedOptions = {};

                    jQuery('div.swatch-attribute').each(function (k,v) {
                        var attribute_id    = jQuery(v).attr('attribute-id');
                        var option_selected = jQuery(v).attr('option-selected');
                        if (!attribute_id || !option_selected) {
                            return;
                        }
                        selectedOptions[attribute_id] = option_selected;
                    });

                    return selectedOptions;
                }

                function getSwatchesOptionsIndex() {
                    return jQuery('[data-role=swatch-options]').data('mageSwatchRenderer').options.jsonConfig.index;
                }

                function isSwatchesProductSelected(attributes, selectedOptions) {
                    return _.isEqual(attributes, selectedOptions);
                }

                function updateSelectedBuyBox() {
                    $('.buybox-radio').each(function() {
                        if($(this).is(':checked'))
                            $(this).click();
                    });
                }

                function debugLog(message) {
                    if(self.debug)
                        console.log(message);
                }

                // Clicks the first radio button
                $("#select-item-0").click();
            });
        },

    });

    $.mpassignproduct.buybox.registerAddToCartValidation = function(validationFunction) {
        addToCartValidations.push(validationFunction);
    }

    $.mpassignproduct.buybox.registerAddToCartExtraJsonData = function(extraJsonDataFunction) {
        addToCartExtraJsonData.push(extraJsonDataFunction);
    }

    return $.mpassignproduct.buybox;
});
